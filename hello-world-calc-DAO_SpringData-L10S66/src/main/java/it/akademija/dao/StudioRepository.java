package it.akademija.dao;

import it.akademija.model.Studio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudioRepository extends JpaRepository<Studio, Long> {

     Studio findByStudioTitle(String studioTitle);
//     List<Studio> findUserByEmailContaining(String partOfEmail);
     void deleteByStudioTitle(String studioTitle);
//     Studio findByFirstNameAndLastName(String firstName, String lastName);




}
