package it.akademija.dao;

import it.akademija.model.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Repository
public class InMemoryUserDao implements UserDao {
    private final List<User> users= new CopyOnWriteArrayList<>();

//    @PostConstruct
//    public void addSomeUsersForTesting(){
//        users.add(new User("name", "sth", "sths", "fs"));
//        users.add(new User("name2", "sth", "sths", "fs"));
//    }

    @Override
    public List<User> getUsers() {
        return Collections.unmodifiableList(users);
    }

    @Override
    public void createUser(User user) {
        users.add(user);
    }

    @Override
    public void deleteUser(String username) {
        for (User user: users){
            if (user.getUsername().equals(username)){
                users.remove(user);
                break;
            }
        }

    }
}
