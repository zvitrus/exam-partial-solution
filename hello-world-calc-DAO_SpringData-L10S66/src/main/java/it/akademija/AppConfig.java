package it.akademija;

import net.sf.log4jdbc.sql.jdbcapi.DataSourceSpy;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
@ImportResource({"classpath*:application-context.xml"})
public class AppConfig {

//    @Bean
//    public Product productBean0() {
//        Product product = new Product();
//        product.setTitle("TitleProductBean0");
//        return product;
//    }
//
//    @Bean
//    public Product productBean1() {
//        Product product = new Product();
//        product.setTitle("TitleProductBean1");
//        return product;
//    }
//    @Bean
//    public Product productBean2(){
//        Product product = new Product();
//        product.setTitle("TitleProductBean2");
//        return  product;
//    }




}