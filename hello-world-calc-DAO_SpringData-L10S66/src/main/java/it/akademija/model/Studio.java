package it.akademija.model;

import javax.persistence.*;

@Entity
public class Studio {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer id;
    @Column
    private String studioTitle;
    @Column
    private String studioLogo;
    @Column
    private String studioCategory;
    @Column
    private String studioSize;

    public Studio(){};

    public Studio(Integer id, String studioTitle, String studioLogo, String studioCategory, String studioSize) {
        this.studioTitle = studioTitle;
        this.studioLogo = studioLogo;
        this.studioCategory = studioCategory;
        this.studioSize = studioSize;
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStudioTitle() {
        return studioTitle;
    }

    public void setStudioTitle(String studioTitle) {
        this.studioTitle = studioTitle;
    }

    public String getStudioLogo() {
        return studioLogo;
    }

    public void setStudioLogo(String studioLogo) {
        this.studioLogo = studioLogo;
    }

    public String getStudioCategory() {
        return studioCategory;
    }

    public void setStudioCategory(String studioCategory) {
        this.studioCategory = studioCategory;
    }

    public String getStudioSize() {
        return studioSize;
    }

    public void setStudioSize(String studioSize) {
        this.studioSize = studioSize;
    }
}
