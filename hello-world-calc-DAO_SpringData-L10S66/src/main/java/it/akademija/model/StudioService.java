package it.akademija.model;

import it.akademija.dao.StudioRepository;
import it.akademija.dao.StudioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudioService {
    @Autowired
    private StudioRepository studioRepository;


    @Transactional(readOnly = true)
    public List<Studio> getStudios(){
        return studioRepository.findAll();
    }
    @Transactional
    public void createStudio(CreateStudioCommand cmd){
        Studio studio = new Studio();
        studio.setStudioTitle(cmd.getStudioTitle());
        studio.setStudioLogo(cmd.getStudioLogo());
        studio.setStudioCategory(cmd.getStudioCategory());
        studio.setStudioSize(cmd.getStudioSize());
        studioRepository.save(studio);
    }
    @Transactional(readOnly = true)
    public Studio getStudioByTitle(String studioTitle){
        return studioRepository.findByStudioTitle(studioTitle);
    }
    @Transactional
    public void removeStudio(String studioTitle){
        studioRepository.deleteByStudioTitle(studioTitle);
    }
//    @Transactional
//    public User getUserByFirstNameAndLastName(String firstName, String lastName){
//        return studioRepository.findByFirstNameAndLastName(firstName, lastName);
//    }

//    public List<Studio> getUsersByPartOfEmail(String partOfEmail) {
//       return  studioRepository.findUserByEmailContaining(partOfEmail);
//    }


//
//
//    @Autowired @Qualifier("repoUserDao")
//    private UserDao userDao;
////
//
//    @Transactional(readOnly = true)
//    public List<User> getUsers(){
//        return userDao.getUsers();
//    }
//    @Transactional
//    public void createUser(User user){
//        userDao.createUser(user);
//    }

}
