package it.akademija;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RestServisas {
    List<Product> productList;

    public List<Product> getProductList() {
        return productList;
    }

    @Autowired
    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @RequestMapping("/hello")
    public String sayHi(){
        return "Hi yourself";
    }

    @RequestMapping("/productsCollection")
    public String getProductsCollection(){
        String productsTitles = "";
        for (Product product: productList){
            productsTitles+= " " + product.getTitle();
        }
        return productsTitles;

    }
    @RequestMapping("/productCollectionList")
    public List<Product> getProductCollectionList(){
        return productList;
    }
    @RequestMapping(method = RequestMethod.GET, value = "/products")
    public List<Product> getProducts(){
        return productList;
    }
    @RequestMapping(method = RequestMethod.POST, value = "/products")
    public void addProduct(@RequestBody Product product){
        productList.add(product);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/products/{title}")
    public void removeProduct(String title){
        productList.removeIf(product -> product.getTitle().equals(title));
    }
}
